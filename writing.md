# writing skills

## technical

2020-10-08 >> [How to set location for Runner logs?](https://forum.gitlab.com/t/how-to-set-location-for-runner-logs/43742)

2018-11-28 >> [GitLab Forum: How to force specific encoding in GitLab UI?](https://forum.gitlab.com/t/how-to-force-specific-encoding-in-gitlab-ui/21333)

2016-02-06 >> [stackoverflow: Unable to remove a value from a text file using -sed](https://stackoverflow.com/a/35237699/3861137)
