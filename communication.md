# general communication skills

# technical communication skills

## bug reporting

2021-04-06 >> [GitLab.com: self monitoring project does not work on RHEL 7](https://gitlab.com/gitlab-org/gitlab/-/issues/326986)

## mentorship

2021-04-07 >> [GitLab.com: [Docs] Fix broken link in the Account and limit GitLab.com settings page](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/58671)
