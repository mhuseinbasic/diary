I am Bosniak, my homeland is Bosnia and Herzegovina.

I love my country and my people.

Men like me used to be executed during [Srebrenica genocide](https://en.wikipedia.org/wiki/Srebrenica_massacre), I was lucky to be born a bit later and in a different city.

People in my country care about nation a lot.

[In 2016](https://www.aa.com.tr/ba/balkan/demografska-slika-balkana-srbija-bih-i-hrvatska-konstantno-bilje%C5%BEe-negativan-prirodni-prira%C5%A1taj/2387886), about 30 thousands people were born in Bosnia and Herzegovina and 35 thousands of them died, so overall population declined by 5 thousands.

For example, [there were 1.7 millions Bosniaks in Bosnia and Herzegovina in 2013](https://bs.wikipedia.org/wiki/Popis_stanovni%C5%A1tva_u_Bosni_i_Hercegovini_2013.#Rezultati). Let's say that 1.2 millions of them were actually spending most of the year in the country. Let's then say that 900 thousands were grown-ups. Ideally, that's 450 thousands couples. Let's imagine that 200 thousands of them can give birth to children. Assuming 1 kid per year, that's 1 more million Bosniaks in 5 years and 4 millions in 20 years. Even if we cut the number in half due to deaths or any other reasons not taken into account here, that's still 2 more millions in 20 years, 4 more millions in 40 years, 6 more millions in 60 years. That's about 11% population growth per year, still less than [12.82% which Qatar had between 2002 and 2011](https://www.worlddata.info/populationgrowth.php?from=2002&to=2011).

To live 60 years is not a big deal, [life expectancy in the US in 2019 was 78.8 years](https://www.healthsystemtracker.org/chart-collection/u-s-life-expectancy-compare-countries/#Life%20expectancy%20at%20birth%20in%20years,%201980-2020%C2%A0). However, with clever geographical distribution of people and by simply using voting power rather than guns, one nation in my country could get significant power for themselves, which is a big deal for many people.  
