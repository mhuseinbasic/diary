### Events

#### 2020-12-20


I spun up Windows Server for the first time in my life. It was [Windows Server 2012 R2](https://en.wikipedia.org/wiki/Windows_Server_2012_R2) on GCP. I installed [GitLab Runner](https://docs.gitlab.com/runner/) on it and run my [jobs](https://docs.gitlab.com/ee/ci/jobs/) with it.
