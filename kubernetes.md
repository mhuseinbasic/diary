### Events

#### 2021-05-06

I worked with OpenShift for the first time in my life. Things I managed to accomplish:
1. Spin up OpenShift cluster on AWS ([ROSA](https://www.openshift.com/products/amazon-openshift)).
2. [Install GitLab Runner inside the OpenShift cluster](https://docs.gitlab.com/runner/install/openshift.html) and run jobs with it.
3. [Configure memory limit within GitLab Runner on OpenShift](ahttps://docs.gitlab.com/runner/executors/kubernetes.html#memory-requests-and-limits). I used [stress-ng](https://manpages.ubuntu.com/manpages/artful/man1/stress-ng.1.html) to verify that the limits work.

#### 2021-04-22

I played around with k8s in my local lab. Here are the hightlights:
1. I installed kubernetes on a local laptop (2 VirtualBox VMs, 1 master node, 1 worker node).
2. I deployed GitLab Runner to this local k8s installation. I authenticated it and run jobs on GitLab.com with it.
3. I hooked up this local cluster to GitLab project and deployed sample nginx service (3 pods) using it.

#### 2020-08-13

I installed GitLab in kubernetes for the first time. I used scripted approach to create the cluster and then contnued on with the installation steps from [GitLab docs (GKE)](https://docs.gitlab.com/charts/installation/cloud/gke.html). I set `USE_STATIC_IP` to true (related to scripted approach) and created wildcard DNS entry in Cloud DNS.

#### 2020-03-19

[My MR related to Helm version check was merged into GitLab Chart repository.](https://gitlab.com/gitlab-org/charts/gitlab/-/merge_requests/1217/diffs)
