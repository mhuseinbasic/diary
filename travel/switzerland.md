# Prices


## Transport (CHF)

2021-11-23

1. Basel Euroairport -> Basel SBB: 6.10
1. Basel SBB -> Lausanne (via Bern o Biel): 65.00
1. Lausanne, gare (30 minutes): 2.30
1. Lausanne-Ouchy -><- Evian les Bains: 36.00

## food (BAM)

2021-11-23

1. croissant: 3.54
1. mint tea to go: 6.90
1. Burger King french fries (medium): 7.30
1. Burger King french fries (small): 6.37

# Food

1. [Raclette](https://en.wikipedia.org/wiki/Raclette)
1. [Carac](https://en.wikipedia.org/wiki/Carac_(pastry))
1. [Mille-feuille](https://en.wikipedia.org/wiki/Carac_(pastry))
